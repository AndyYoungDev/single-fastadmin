<?php

return [
    'Icon'       => '小图标',
    'Url'        => '跳转地址',
    'Title'      => '显示名称',
    'Created_at' => '创建时间',
    'Updated_at' => '操作时间'
];
