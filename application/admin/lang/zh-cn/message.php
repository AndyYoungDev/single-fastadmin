<?php

return [
    'Company'    => '公司名称',
    'Name'       => '联系人',
    'Phone'      => '电话',
    'Email'      => '邮箱',
    'Msg'        => '留言',
    'Created_at' => '创建时间',
    'Updated_at' => '操作时间'
];
