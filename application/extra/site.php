<?php

return [
    'name' => 'xxxx设备有限公司',
    'beian' => '豫A888888',
    'forbiddenip' => '',
    'fixedpage' => 'dashboard',
    'desc' => '描述',
    'keywords' => '关键词，关键词2',
    'logo' => '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg',
    'banner' => '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg',
    'admin_email' => '123456789@qq.com',
    'configgroup' => [
        'basic' => 'Basic',
        'about' => '关于我们',
        'dictionary' => 'Dictionary',
    ],
    'about_img' => '/uploads/20210105/12dfd8f5f3ff2f9b176d3bd998dc919d.jpg',
    'FAQ' => '',
    'about_text' => '无',
    'footer_img' => '/uploads/20210106/e1751ab5a7452b0dbbdf6ec462903e69.jpg',
    'honour_img' => '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg',
    'packing_img' => '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg',
    'statistics' => '<script></script>',
    'device_img' => '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg',
];
