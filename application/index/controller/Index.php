<?php

namespace app\index\controller;

use app\admin\model\About;
use app\admin\model\Article;
use app\admin\model\Icon;
use app\admin\model\Image;
use app\admin\model\Message;
use app\common\controller\Frontend;
use think\Request;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index(Request $request)
    {



        if ($request->isPost()){
            $param=$request->post();
            if(!captcha_check($param['captcha'])){
                echo "<script>alert('验证码错误');window.location.href='/'</script>";die;
            };
            $model=new Message();
            unset($param['captcha']);
            $model->save($param);
        }






        $icon=Icon::all();
        $aboutImgs=Image::where("category","关于我们")->limit(3)->select();
        $proImgs=Image::where("category","生产设备")->limit(6)->select();
        $HonourImgs=Image::where("category","荣誉资质")->limit(3)->select();
        $proDetail=Article::where("category","产品详情")->limit(1)->select();
        $productImgs=Image::where("category","产品详情")->select();
        $faqDetail=Article::where("category","faq")->limit(1)->select();
        $aboutDetail=Article::where("category","关于我们")->limit(1)->select();
        $aboutFour=About::limit(4)->select();

        if (!empty($proDetail)){
            $proDetail=$proDetail[0];
        }
        if (!empty($faqDetail)){
            $faqDetail=$faqDetail[0];
        }
        if (!empty($aboutDetail)){
            $aboutDetail=$aboutDetail[0];
        }

        $this->assign('icon',$icon);
        $this->assign('aboutImgs',$aboutImgs);
        $this->assign('proImgs',$proImgs);
        $this->assign('HonourImgs',$HonourImgs);
        $this->assign('proArticle',$proDetail);
        $this->assign('productImgs',$productImgs);
        $this->assign('faqArticle',$faqDetail);
        $this->assign('aboutArticle',$aboutDetail);
        $this->assign('aboutFour',$aboutFour);
        return $this->view->fetch();
    }

}
