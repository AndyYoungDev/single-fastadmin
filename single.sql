-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2021-01-06 14:46:36
-- 服务器版本： 5.6.50-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `single`
--

-- --------------------------------------------------------

--
-- 表的结构 `fa_about`
--

CREATE TABLE IF NOT EXISTS `fa_about` (
  `id` int(11) NOT NULL,
  `p1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文字1',
  `p2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文字2',
  `p3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文字3',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='关于我们';

--
-- 转存表中的数据 `fa_about`
--

INSERT INTO `fa_about` (`id`, `p1`, `p2`, `p3`, `created_at`, `updated_at`) VALUES
(1, '国家 COUNTRY', '我们的产品销往全国20多个国家', 'Our products are sold to more than 20 countries across the country.', '2021-01-05 03:21:17', '2021-01-05 11:40:38'),
(2, '产品 PRODUCT', '10年专注电容器制造研发。', 'Focus on the manufacturing and development of capacitors for ten years.', '2021-01-05 03:21:33', '2021-01-05 12:06:20'),
(3, '技术 TECHNOLOGY', '拥有专业的技术团队和售后服务。', 'Have a professional technical team and after-sales service.', '2021-01-05 03:21:17', '2021-01-05 11:39:56'),
(4, '品牌 BRAND', '成立于2006年，自主品牌及OEM贴牌代加工。', 'Founded in 2006, independent brand and OEM processing.', '2021-01-05 03:21:33', '2021-01-05 12:07:42');

-- --------------------------------------------------------

--
-- 表的结构 `fa_admin`
--

CREATE TABLE IF NOT EXISTS `fa_admin` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员表';

--
-- 转存表中的数据 `fa_admin`
--

INSERT INTO `fa_admin` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`, `loginfailure`, `logintime`, `loginip`, `createtime`, `updatetime`, `token`, `status`) VALUES
(1, 'admin', 'Admin', 'f90f2266b102ecfbe541287899d1120d', 'p6bUhv', '/assets/img/avatar.png', 'admin@admin.com', 0, 1609905667, '101.93.98.41', 1492186163, 1609905667, 'dd234c87-2593-4884-89af-e90c2260220c', 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_admin_log`
--

CREATE TABLE IF NOT EXISTS `fa_admin_log` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '日志标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员日志表';

--
-- 转存表中的数据 `fa_admin_log`
--

INSERT INTO `fa_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(1, 1, 'admin', '/rnZVRMNQGX.php/general.profile/update', '常规管理 / 个人资料 / 更新个人信息', '{"__token__":"***","row":{"avatar":"\\/assets\\/img\\/avatar.png","email":"admin@admin.com","nickname":"Admin","password":"***"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609725916),
(2, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"My Website","beian":"","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726075),
(3, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/edit/ids/6?dialog=1', '权限管理 / 菜单规则 / 编辑', '{"dialog":"1","__token__":"***","row":{"ismenu":"1","pid":"0","name":"general\\/config","title":"系统配置","icon":"fa fa-cog","weigh":"60","condition":"","remark":"Config tips","status":"normal"},"ids":"6"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726142),
(4, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"2","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726145),
(5, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"3","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726148),
(6, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"6","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726152),
(7, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"6","params":"ismenu=1"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726153),
(8, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"4","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726154),
(9, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"66","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726155),
(10, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726273),
(11, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"My Website","beian":"","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726284),
(12, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"My Website","beian":"","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726371),
(13, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"test","beian":"","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726491),
(14, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726586),
(15, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609726635),
(16, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/del', '权限管理 / 菜单规则 / 删除', '{"action":"del","ids":"85","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727173),
(17, 1, 'admin', '/rnZVRMNQGX.php/index/login', '登录', '{"__token__":"***","username":"admin","password":"***","captcha":"ebcz"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727197),
(18, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/del', '权限管理 / 菜单规则 / 删除', '{"action":"del","ids":"95","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727221),
(19, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727325),
(20, 1, 'admin', '/rnZVRMNQGX.php/icon/add?dialog=1', '图标管理 / 添加', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/0ede3d0a26f5d5668187b401a8a5eb81.png","url":"www.baidu.com","title":"小图标","created_at":"2021-01-04 10:28:40","updated_at":"2021-01-04 10:28:40"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727336),
(21, 1, 'admin', '/rnZVRMNQGX.php/icon/del', '图标管理 / 删除', '{"action":"del","ids":"1","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727589),
(22, 1, 'admin', '/rnZVRMNQGX.php/icon/add?dialog=1', '图标管理 / 添加', '{"dialog":"1","row":{"icon":"\\/assets\\/img\\/qrcode.png","url":"www.baidu.com","title":"小图标","created_at":"2021-01-04 10:33:10","updated_at":"2021-01-04 10:33:10"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727597),
(23, 1, 'admin', '/rnZVRMNQGX.php/icon/del', '图标管理 / 删除', '{"action":"del","ids":"2","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727783),
(24, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727788),
(25, 1, 'admin', '/rnZVRMNQGX.php/icon/add?dialog=1', '图标管理 / 添加', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/a4b9b78965325a2babe7a5d440df765a.png","url":"www.baidu.com","title":"小图标","created_at":"2021-01-04 10:36:23","updated_at":"2021-01-04 10:36:23"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727791),
(26, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727832),
(27, 1, 'admin', '/rnZVRMNQGX.php/icon/edit/ids/3?dialog=1', '图标管理 / 编辑', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/7f17e4dfe9cea648d6cd090a684f78fe.png","url":"www.baidu.com","title":"小图标","created_at":"2021-01-04 10:36:23","updated_at":"2021-01-04 10:36:23"},"ids":"3"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727834),
(28, 1, 'admin', '/rnZVRMNQGX.php/icon/add?dialog=1', '图标管理 / 添加', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/7f17e4dfe9cea648d6cd090a684f78fe.png","url":"www.baidu.com","title":"小图标","created_at":"2021-01-04 10:37:48","updated_at":"2021-01-04 10:37:48"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609727874),
(29, 1, 'admin', '/rnZVRMNQGX.php/icon/edit/ids/4?dialog=1', '图标管理 / 编辑', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/7f17e4dfe9cea648d6cd090a684f78fe.png","url":"http:\\/\\/www.baidu.com","title":"小图标","created_at":"2021-01-04 10:37:48","updated_at":"2021-01-04 10:37:48"},"ids":"4"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728300),
(30, 1, 'admin', '/rnZVRMNQGX.php/icon/edit/ids/3?dialog=1', '图标管理 / 编辑', '{"dialog":"1","row":{"icon":"\\/uploads\\/20210104\\/7f17e4dfe9cea648d6cd090a684f78fe.png","url":"http:\\/\\/www.baidu.com","title":"小图标","created_at":"2021-01-04 10:36:23","updated_at":"2021-01-04 10:37:14"},"ids":"3"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728304),
(31, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728443),
(32, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/fb473e4fd3df0354e4aa8478ee0dfab5.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728461),
(33, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728717),
(34, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/34550b001401852b726342469da3b7d7.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728778),
(35, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728826),
(36, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/assets\\/img\\/qrcode.png","banner":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728827),
(37, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728882),
(38, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728923),
(39, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"283598349@qq.com"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609728924),
(40, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"configgroup":"{&quot;basic&quot;:&quot;Basic&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;,&quot;about&quot;:&quot;About&quot;}"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609729945),
(41, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"configgroup":"{&quot;basic&quot;:&quot;Basic&quot;,&quot;about&quot;:&quot;About&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;}"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609729963),
(42, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"configgroup":"{&quot;basic&quot;:&quot;Basic&quot;,&quot;about&quot;:&quot;关于我们&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;}"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609729979),
(43, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"configgroup":"{&quot;basic&quot;:&quot;Basic&quot;,&quot;about&quot;:&quot;关于我们&quot;,&quot;faq&quot;:&quot;FAQ&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;}"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609730169),
(44, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609731269),
(45, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609731285),
(46, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。\\r\\n郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609731336),
(47, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609731403),
(48, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210104\\/ab26520f41a81ef5eb3ff3a921fffdc8.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。\\r\\n郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609731404),
(49, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"3","params":"ismenu=1"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609743032),
(50, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"3","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609743050),
(51, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609743884),
(52, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609743936),
(53, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609743937),
(54, 1, 'admin', '/rnZVRMNQGX.php/image/dropdown', '图片管理', '{"q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744397),
(55, 1, 'admin', '/rnZVRMNQGX.php/image/dropdown', '图片管理', '{"q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744430),
(56, 1, 'admin', '/rnZVRMNQGX.php/image/dropdown', '图片管理', '{"q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744431),
(57, 1, 'admin', '/rnZVRMNQGX.php/image/dropdown', '图片管理', '{"q_word":[""],"pageNumber":"1","pageSize":"10","andOr":"OR ","orderBy":[["name","ASC"]],"searchTable":"tbl","showField":"name","keyField":"id","searchField":["name"],"name":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744432),
(58, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744551),
(59, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"关于我们","image":"\\/uploads\\/20210104\\/bd5816dc48eb4165b0363cad44834d98.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:14:49","updated_at":"2021-01-04 15:14:49"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744566),
(60, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744617),
(61, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"关于我们","image":"\\/uploads\\/20210104\\/bd5816dc48eb4165b0363cad44834d98.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:16:50","updated_at":"2021-01-04 15:16:50"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609744621),
(62, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"关于我们","image":"\\/uploads\\/20210104\\/bd5816dc48eb4165b0363cad44834d98.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:28:44","updated_at":"2021-01-04 15:28:44"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745334),
(63, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745457),
(64, 1, 'admin', '/rnZVRMNQGX.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"283598349@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745464),
(65, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745868),
(66, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"生产设备","image":"\\/uploads\\/20210104\\/3dd73a99cc1d7329fdc1db20378eed2b.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:37:21","updated_at":"2021-01-04 15:37:21"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745871),
(67, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"生产设备","image":"\\/uploads\\/20210104\\/3dd73a99cc1d7329fdc1db20378eed2b.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:37:53","updated_at":"2021-01-04 15:37:53"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609745884),
(68, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746617),
(69, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746629),
(70, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210104\\/e2b90caa6a24516794d9629b8bf38e65.png","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:50:08","updated_at":"2021-01-04 15:50:08"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746633),
(71, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746803),
(72, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210104\\/8b97b628a9148e6c0e876de6d42bd6f6.jpg","p1":"","p2":"","created_at":"2021-01-04 15:53:14","updated_at":"2021-01-04 15:53:14"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746805),
(73, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746831),
(74, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210104\\/2332cee929b416037e7fd712857ab02a.jpg","p1":"","p2":"","created_at":"2021-01-04 15:53:45","updated_at":"2021-01-04 15:53:45"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609746833),
(75, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/del', '权限管理 / 菜单规则 / 删除', '{"action":"del","ids":"116","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609747368),
(76, 1, 'admin', '/rnZVRMNQGX.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"4","params":"ismenu=1"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609749719),
(77, 1, 'admin', '/rnZVRMNQGX.php/addon/install', '插件管理', '{"name":"summernote","force":"0","uid":"39265","token":"***","version":"1.0.4","faversion":"1.2.0.20201008_beta"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609749786),
(78, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609749883),
(79, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609749898),
(80, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609749915),
(81, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:47:17","updated_at":"2021-01-04 16:47:17"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750054),
(82, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:47:17","updated_at":"2021-01-04 16:47:17"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750096),
(83, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:47:17","updated_at":"2021-01-04 16:47:17"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750100),
(84, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:47:17","updated_at":"2021-01-04 16:47:17"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750139),
(85, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750482),
(86, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750491),
(87, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:54:37","updated_at":"2021-01-04 16:54:37"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750500),
(88, 1, 'admin', '/rnZVRMNQGX.php/article/del', '文本管理 / 删除', '{"action":"del","ids":"1","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750502),
(89, 1, 'admin', '/rnZVRMNQGX.php/article/del', '文本管理 / 删除', '{"action":"del","ids":"2","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750540),
(90, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750592),
(91, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750601),
(92, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:55:41","updated_at":"2021-01-04 16:55:41"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750612),
(93, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750644),
(94, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750654),
(95, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","content":"","created_at":"2021-01-04 16:57:20","updated_at":"2021-01-04 16:57:20"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750702),
(96, 1, 'admin', '/rnZVRMNQGX.php/article/del', '文本管理 / 删除', '{"action":"del","ids":"3","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750713),
(97, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750799),
(98, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210104\\/fa13f1ce9d1889a21571b1d759f7d273.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609750803),
(99, 1, 'admin', '/rnZVRMNQGX.php/ajax/upload', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609751456),
(100, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"物流运输","image":"\\/uploads\\/20210104\\/8ff733b93e223017ab43e47e85ff319a.jpg","p1":"","p2":"","created_at":"2021-01-04 17:10:37","updated_at":"2021-01-04 17:10:37"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609751458),
(101, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"物流运输","image":"\\/uploads\\/20210104\\/8ff733b93e223017ab43e47e85ff319a.jpg","p1":"","p2":"","created_at":"2021-01-04 17:11:00","updated_at":"2021-01-04 17:11:00"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609751466),
(102, 1, 'admin', '/rnZVRMNQGX.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"物流运输","image":"\\/uploads\\/20210104\\/8ff733b93e223017ab43e47e85ff319a.jpg","p1":"","p2":"","created_at":"2021-01-04 17:11:07","updated_at":"2021-01-04 17:11:07"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609751473),
(103, 1, 'admin', '/rnZVRMNQGX.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"FAQ","content":"Q: Are you a factory or trading company? A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. Q: What''s your main products? A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. Q: How does your factory do regarding quality control? A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal test during production, and the third test before packing. Q: How do you ship the goods and how long does it take to arrive? A: We usually ship by vessel. It according to vessel. Q: How long is your delivery time? A: Generally it is 15-30 days, it''s according to quantity. Q: Do you provide samples ? is it free or extra ? A: Yes, we could offer the sample for free charge. Q: What is your terms of payment ? A: Payment&amp;lt;=1000USD, 100% in advance. Payment&amp;gt;=1000USD, 30% T\\/T in advance ,balance before shippment. If you have another question, please feel free to contact us as below.","created_at":"2021-01-04 17:27:59","updated_at":"2021-01-04 17:27:59"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609752497),
(104, 0, 'Unknown', '/admin.php/index/login?url=%2Fadmin.php', '', '{"url":"\\/admin.php","__token__":"***","username":"admin","password":"***","captcha":"xmdb"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811418),
(105, 1, 'admin', '/admin.php/index/login?url=%2Fadmin.php', '登录', '{"url":"\\/admin.php","__token__":"***","username":"admin","password":"***","captcha":"x4va"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811426),
(106, 1, 'admin', '/backend.php/auth/rule/del', '权限管理 / 菜单规则 / 删除', '{"action":"del","ids":"130","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811555),
(107, 1, 'admin', '/backend.php/auth/rule/del', '权限管理 / 菜单规则 / 删除', '{"action":"del","ids":"137","params":""}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811609),
(108, 1, 'admin', '/backend.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"4","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811662),
(109, 1, 'admin', '/backend.php/auth/rule/multi', '权限管理 / 菜单规则', '{"action":"","ids":"1","params":"ismenu=0"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811692),
(110, 1, 'admin', '/backend.php/ajax/weigh', '', '{"ids":"6,1,2,7,8,3,5,9,10,11,12,4,66,67,73,79,102,109,123,144","changeid":"6","pid":"0","field":"weigh","orderway":"desc","table":"auth_rule","pk":"id"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811717),
(111, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"configgroup":"{&quot;basic&quot;:&quot;Basic&quot;,&quot;about&quot;:&quot;关于我们&quot;,&quot;dictionary&quot;:&quot;Dictionary&quot;}"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609811755),
(112, 1, 'admin', '/backend.php/article/add?dialog=1', '文本管理 / 添加', '{"dialog":"1","row":{"category":"关于我们","content":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 10:25:15"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609813524),
(113, 1, 'admin', '/backend.php/about/add?dialog=1', '关于我们 / 添加', '{"dialog":"1","row":{"p1":"20+","p2":"国家","p3":"我们的产品销往全国20多个国家","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 11:21:17"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609816890),
(114, 1, 'admin', '/backend.php/about/add?dialog=1', '关于我们 / 添加', '{"dialog":"1","row":{"p1":"30+","p2":"国家","p3":"我们的产品销往全国20多个国家","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 11:21:33"}}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609816905),
(115, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"H6PK"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609818946),
(116, 1, 'admin', '/backend.php/image/edit/ids/24?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210104\\/fa13f1ce9d1889a21571b1d759f7d273.jpg","p1":"TEST","p2":"先进的生产设备","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"},"ids":"24"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609819228),
(117, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609819288),
(118, 1, 'admin', '/backend.php/image/add?dialog=1', '图片管理 / 添加', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210105\\/ee45c819d46c42812c02a84ea52b6d4e.jpg","p1":"TEST 2","p2":"测试2","created_at":"2021-01-05 12:00:40","updated_at":"2021-01-05 12:00:40"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609819303),
(119, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609820589),
(120, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/15b8a5023701d6063ab5178223941c5a.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609820591),
(121, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/15b8a5023701d6063ab5178223941c5a.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609820597),
(122, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609820754),
(123, 1, 'admin', '/backend.php/about/edit/ids/1?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"20+国家","p2":"我们的产品销往全国20多个国家","p3":"Our products are sold to more than 20 countries around the world.","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 11:21:17"},"ids":"1"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609823099),
(124, 1, 'admin', '/backend.php/about/edit/ids/1?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"国家 COUNTRY","p2":"我们的产品销往全国20多个国家","p3":"Our products are sold to more than 20 countries ar","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 13:04:59"},"ids":"1"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609823208),
(125, 1, 'admin', '/backend.php/about/edit/ids/2?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"产品 PRODUCT","p2":"提供专业的产品，涵盖家电、机械等行业。","p3":"Provide professional products, covering household appliances, machinery and other industries.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 11:21:33"},"ids":"2"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609823332),
(126, 1, 'admin', '/backend.php/about/edit/ids/3?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"技术 TECHNOLOGY","p2":"拥有专业的技术团队和售后服务。","p3":"Have a professional technical team and after-sales service.","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 11:21:17"},"ids":"3"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609823492),
(127, 1, 'admin', '/backend.php/about/edit/ids/4?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"品牌 BRAND","p2":"成立于2006年，自主品牌。","p3":"Founded in 2006, independent brand.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 11:21:33"},"ids":"4"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609823610),
(128, 1, 'admin', '/backend.php/message/del', '留言管理 / 删除', '{"action":"del","ids":"2","params":""}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609824459),
(129, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609824515),
(130, 1, 'admin', '/backend.php/image/edit/ids/12?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210105\\/9f80c627bdb192ef4a7ca0a641b35ad5.jpg","p1":"","p2":"","created_at":"2021-01-04 15:53:45","updated_at":"2021-01-04 15:53:45"},"ids":"12"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609824516),
(131, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"endc"}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609829563),
(132, 1, 'admin', '/backend.php/article/edit/ids/5?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"FAQ","content":"Q: Are you a factory or trading company? A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. Q: What''s your main products? A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. Q: How does your factory do regarding quality control? A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal test during production, and the third test before packing. Q: How do you ship the goods and how long does it take to arrive? A: We usually ship by vessel. It according to vessel. Q: How long is your delivery time? A: Generally it is 15-30 days, it''s according to quantity. Q: Do you provide samples ? is it free or extra ? A: Yes, we could offer the sample for free charge. Q: What is your terms of payment ? A: Payment&amp;lt;=1000USD, 100% in advance. Payment&amp;gt;=1000USD, 30% T\\/T in advance ,balance before shippment. If you have another question, please feel free to contact us as below.","created_at":"2021-01-04 17:27:59","updated_at":"2021-01-04 17:27:59"},"ids":"5"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609829743),
(133, 1, 'admin', '/backend.php/article/edit/ids/5?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"FAQ","content":"Q: Are you a factory or trading company? A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. Q: What''s your main products? A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. Q: How does your factory do regarding quality control? A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal test during production, and the third test before packing. Q: How do you ship the goods and how long does it take to arrive? A: We usually ship by vessel. It according to vessel. Q: How long is your delivery time? A: Generally it is 15-30 days, it''s according to quantity. Q: Do you provide samples ? is it free or extra ? A: Yes, we could offer the sample for free charge. Q: What is your terms of payment ? A: Payment&amp;lt;=1000USD, 100% in advance. Payment&amp;gt;=1000USD, 30% T\\/T in advance ,balance before shippment. If you have another question, please feel free to contact us as below.","created_at":"2021-01-04 17:27:59","updated_at":"2021-01-05 14:55:43"},"ids":"5"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609829793);
INSERT INTO `fa_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(134, 1, 'admin', '/backend.php/article/edit/ids/5?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"FAQ","content":"Q: Are you a factory or trading company? A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. Q: What''s your main products? A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. Q: How does your factory do regarding quality control? A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal test during production, and the third test before packing. Q: How do you ship the goods and how long does it take to arrive? A: We usually ship by vessel. It according to vessel. Q: How long is your delivery time? A: Generally it is 15-30 days, it''s according to quantity. Q: Do you provide samples ? is it free or extra ? A: Yes, we could offer the sample for free charge. Q: What is your terms of payment ? A: Payment&amp;lt;=1000USD, 100% in advance. Payment&amp;gt;=1000USD, 30% T\\/T in advance ,balance before shippment. If you have another question, please feel free to contact us as below.","created_at":"2021-01-04 17:27:59","updated_at":"2021-01-05 14:56:33"},"ids":"5"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609829998),
(135, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609830157),
(136, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg"}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609830161),
(137, 1, 'admin', '/backend.php/about/edit/ids/4?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"品牌 BRAND","p2":"成立于2006年，自主品牌。1","p3":"Founded in 2006, independent brand.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 13:13:30"},"ids":"4"}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609830194),
(138, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"alert(1)"}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609830455),
(139, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":""}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609830462),
(140, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"W2QQ"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609833467),
(141, 1, 'admin', '/backend.php/article/edit/ids/5?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"FAQ","content":"Q: Are you a factory or trading company? A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. Q: What''s your main products? A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. Q: How does your factory do regarding quality control? A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal&amp;nbsp;test during production, and the third test before packing.Q: How do you ship the goods and how long does it take to arrive? A: We usually ship by vessel. It according to vessel. Q: How long is your delivery time? A: Generally it is 15-30 days, it''s according to quantity. Q: Do you provide samples ? is it free or extra ? A: Yes, we could offer the sample for free charge. Q: What is your terms of payment ? A: Payment&amp;lt;=1000USD, 100% in advance. Payment&amp;gt;=1000USD, 30% T\\/T in advance ,balance before shippment. If you have another question, please feel free to contact us as below.","created_at":"2021-01-04 17:27:59","updated_at":"2021-01-05 14:59:58"},"ids":"5"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609835185),
(142, 1, 'admin', '/backend.php/article/edit/ids/4?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","content":"123456789","created_at":"2021-01-04 16:57:20","updated_at":"2021-01-04 16:57:20"},"ids":"4"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609835542),
(143, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836368),
(144, 1, 'admin', '/backend.php/image/edit/ids/13?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210105\\/88c53449a7cddb06af7662bec707838c.jpg","p1":"先进的生产设备","p2":"Advanced production equipment","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"},"ids":"13"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836386),
(145, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836432),
(146, 1, 'admin', '/backend.php/image/edit/ids/14?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210105\\/88c53449a7cddb06af7662bec707838c.jpg","p1":"先进的生产设备","p2":"Advanced production equipment","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"},"ids":"14"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836447),
(147, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836467),
(148, 1, 'admin', '/backend.php/image/edit/ids/15?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210105\\/88c53449a7cddb06af7662bec707838c.jpg","p1":"先进的生产设备","p2":"Advanced production equipment","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"},"ids":"15"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836475),
(149, 1, 'admin', '/backend.php/image/edit/ids/16?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"产品详情","image":"\\/uploads\\/20210105\\/88c53449a7cddb06af7662bec707838c.jpg","p1":"先进的生产设备","p2":"Advanced production equipment","created_at":"2021-01-04 16:59:35","updated_at":"2021-01-04 16:59:35"},"ids":"16"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836498),
(150, 1, 'admin', '/backend.php/about/edit/ids/4?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"品牌 BRAND","p2":"成立于2006年，自主品牌。","p3":"Founded in 2006, independent brand.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 15:03:14"},"ids":"4"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609836735),
(151, 1, 'admin', '/backend.php/article/edit/ids/6?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"关于我们","content":"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 10:25:15"},"ids":"6"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609837387),
(152, 1, 'admin', '/backend.php/article/edit/ids/6?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"关于我们","content":"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 17:03:07"},"ids":"6"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609837425),
(153, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609837567),
(154, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/9eb738f4917e2956b157e7f2d693e79f.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609837570),
(155, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/9eb738f4917e2956b157e7f2d693e79f.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609837578),
(156, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838056),
(157, 1, 'admin', '/backend.php/article/edit/ids/6?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"关于我们","content":"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 17:03:45"},"ids":"6"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609838122),
(158, 1, 'admin', '/backend.php/article/edit/ids/6?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"category":"关于我们","content":"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 17:15:22"},"ids":"6"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609838166),
(159, 1, 'admin', '/backend.php/article/edit/ids/6?dialog=1', '文本管理 / 编辑', '{"dialog":"1","row":{"content":"&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。","created_at":"2021-01-05 10:25:15","updated_at":"2021-01-05 17:16:06"},"ids":"6"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609838214),
(160, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838222),
(161, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/79414a31813e147cedd1d071bbf0e406.jpg"}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838224),
(162, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838453),
(163, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/79414a31813e147cedd1d071bbf0e406.jpg"}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838454),
(164, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838472),
(165, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210104\\/888105480ed88c274aa6c8ae03dc64a3.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/79414a31813e147cedd1d071bbf0e406.jpg"}}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609838473),
(166, 1, 'admin', '/backend.php/about/edit/ids/1?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"国家 COUNTRY","p2":"我们的产品销往全国20多个国家","p3":"Our products are sold to more than 20 countries ar11111","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 13:06:48"},"ids":"1"}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609839165),
(167, 1, 'admin', '/backend.php/about/edit/ids/3?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"技术 TECHNOLOGY","p2":"拥有专业的技术团队和售后服务。","p3":"Have a professional technical team and after-sales service.","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 13:11:32"},"ids":"3"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609846796),
(168, 1, 'admin', '/backend.php/about/edit/ids/1?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"国家 COUNTRY","p2":"我们的产品销往全国20多个国家","p3":"Our products are sold to more than 20 countries across the country.","created_at":"2021-01-05 11:21:17","updated_at":"2021-01-05 17:32:45"},"ids":"1"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609846838),
(169, 1, 'admin', '/backend.php/about/edit/ids/2?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"产品 PRODUCT","p2":"提供专业的产品，涵盖家电、机械等行业。","p3":"Provide professional products, covering household appliances, machinery and other industries.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 13:08:52"},"ids":"2"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609846881),
(170, 1, 'admin', '/backend.php/about/edit/ids/2?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"产品 PRODUCT","p2":"提供专业的产品，涵盖家电、机械等行业。","p3":"Products are suitable for household appliances, machinery and equipment industries.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 19:41:21"},"ids":"2"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609846940),
(171, 1, 'admin', '/backend.php/about/edit/ids/2?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"产品 PRODUCT","p2":"10年专注电容器制造研发。","p3":"Focus on the manufacturing and development of capacitors for ten years.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 19:42:20"},"ids":"2"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848380),
(172, 1, 'admin', '/backend.php/about/edit/ids/4?dialog=1', '关于我们 / 编辑', '{"dialog":"1","row":{"p1":"品牌 BRAND","p2":"成立于2006年，自主品牌及OEM贴牌代加工。","p3":"Founded in 2006, independent brand and OEM processing.","created_at":"2021-01-05 11:21:33","updated_at":"2021-01-05 16:52:15"},"ids":"4"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848462),
(173, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848562),
(174, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848677),
(175, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/c23f3f59eb2e349622644accc569f061.png","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848681),
(176, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848936),
(177, 1, 'admin', '/backend.php/image/edit/ids/11?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210105\\/be28eb0e6c800a654755c451fd21b217.jpg","p1":"","p2":"","created_at":"2021-01-04 15:53:14","updated_at":"2021-01-04 15:53:14"},"ids":"11"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609848941),
(178, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609849846),
(179, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/12dfd8f5f3ff2f9b176d3bd998dc919d.jpg","about_text":"郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。\\r\\n\\r\\n\\r\\n郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609849848),
(180, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"P8UN"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851586),
(181, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851604),
(182, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851643),
(183, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210105\\/057c884acd2c865acbe741a039bb859e.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/25ad904a44365947d057a8e687d675ab.jpg"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851645),
(184, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851890),
(185, 1, 'admin', '/backend.php/image/edit/ids/10?dialog=1', '图片管理 / 编辑', '{"dialog":"1","row":{"category":"荣誉资质","image":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","p1":"先进的生产设备","p2":"先进的生产设备","created_at":"2021-01-04 15:50:08","updated_at":"2021-01-04 15:50:08"},"ids":"10"}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609851893),
(186, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609852424),
(187, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210104\\/763ddb073c4e17b61380f5f89f1dfcb3.jpg","banner":"\\/uploads\\/20210105\\/846e600751f159a7553d11cefef294d0.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/25ad904a44365947d057a8e687d675ab.jpg"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609852432),
(188, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609853556),
(189, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/846e600751f159a7553d11cefef294d0.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/25ad904a44365947d057a8e687d675ab.jpg"}}', '118.134.165.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609853561),
(190, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"ykwi"}', '123.52.22.188', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898453),
(191, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"vbcr"}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898476),
(192, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/846e600751f159a7553d11cefef294d0.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/1dc94bb480e19fa7ec6af2dce1b02098.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg"}}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898624),
(193, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/846e600751f159a7553d11cefef294d0.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","honour_img":"\\/uploads\\/20210104\\/a5338098974474a991d6538edeea084c.jpg","packing_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg"}}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898648),
(194, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","honour_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","packing_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg"}}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898707),
(195, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","honour_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","packing_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg"}}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898740),
(196, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"about_img":"\\/uploads\\/20210105\\/12dfd8f5f3ff2f9b176d3bd998dc919d.jpg","about_text":"无"}}', '39.162.141.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609898784),
(197, 0, 'Unknown', '/backend.php/index/login?url=%2Fbackend.php', '', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"UYUM"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609905653),
(198, 1, 'admin', '/backend.php/index/login?url=%2Fbackend.php', '登录', '{"url":"\\/backend.php","__token__":"***","username":"admin","password":"***","captcha":"rk6x"}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609905667),
(199, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","honour_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","packing_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/25ad904a44365947d057a8e687d675ab.jpg"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609905791),
(200, 1, 'admin', '/backend.php/ajax/upload', '', '[]', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609905909),
(201, 1, 'admin', '/backend.php/general.config/edit', '常规管理 / 系统配置 / 编辑', '{"__token__":"***","row":{"name":"xxxx设备有限公司","beian":"豫A888888","forbiddenip":"","fixedpage":"dashboard","desc":"描述","keywords":"关键词，关键词2","logo":"\\/uploads\\/20210105\\/5d4702b79657455a8daf2415022388b4.jpg","banner":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","admin_email":"123456789@qq.com","footer_img":"\\/uploads\\/20210106\\/e1751ab5a7452b0dbbdf6ec462903e69.jpg","honour_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","packing_img":"\\/uploads\\/20210105\\/cf67af91f034f49b1a1f774d71423dbb.jpg","statistics":"","device_img":"\\/uploads\\/20210105\\/25ad904a44365947d057a8e687d675ab.jpg"}}', '101.93.98.41', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609905911);

-- --------------------------------------------------------

--
-- 表的结构 `fa_area`
--

CREATE TABLE IF NOT EXISTS `fa_area` (
  `id` int(10) NOT NULL COMMENT 'ID',
  `pid` int(10) DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简称',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) DEFAULT NULL COMMENT '层级 0 1 2 省市区县',
  `pinyin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纬度'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='地区表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_article`
--

CREATE TABLE IF NOT EXISTS `fa_article` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '富文本',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文本管理';

--
-- 转存表中的数据 `fa_article`
--

INSERT INTO `fa_article` (`id`, `category`, `content`, `created_at`, `updated_at`) VALUES
(5, 'FAQ', '<div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: Are you a factory or trading company? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: We are professional manufacture established in 2006 for the purpose of providing top quality capacitors. </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: What''s your main products? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: CBB60capacitor, CBB61capacitor, CBB65capacitor, CBB80capacitor etc. </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: How does your factory do regarding quality control? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: Quality is priority. our factory do 100% inspection at each production procedure, and we do 2 rounds personal&nbsp;</span><span style="font-family: Arial; font-size: 24px;">test during production, and the third test before packing.</span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: How do you ship the goods and how long does it take to arrive? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: We usually ship by vessel. It according to vessel. </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: How long is your delivery time? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: Generally it is 15-30 days, it''s according to quantity. </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: Do you provide samples ? is it free or extra ? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: Yes, we could offer the sample for free charge. </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial; background-color: rgb(255, 255, 0);">Q: What is your terms of payment ? </span></div><div style="line-height: 1.5;"><span style="font-size: 24px; font-family: Arial;">A: Payment&lt;=1000USD, 100% in advance. Payment&gt;=1000USD, 30% T/T in advance ,balance before shippment. </span></div><div style="line-height: 1.5;"><span style="font-family: Arial; font-size: 24px; background-color: rgb(0, 0, 255); color: rgb(255, 255, 255);">If you have another question, please feel free to contact us as below.</span></div>', '2021-01-04 09:27:59', '2021-01-05 08:26:25'),
(4, '产品详情', '<p><img src="/uploads/20210104/c5713d6128685bb772e6b48ad9f0f376.jpg" data-filename="filename" style="width: 100%;"></p><p><br></p><table class="table table-bordered"><tbody><tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr></tbody></table><p><br></p>', '2021-01-04 08:57:20', '2021-01-05 08:32:22'),
(6, '关于我们', '<p style="line-height: 2.8;">&nbsp; &nbsp; &nbsp; &nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。</p><p style="line-height: 2;">&nbsp; &nbsp; &nbsp; &nbsp; 郑州大学大学科技园是学校创建“双一流”、完善科技创新体系的重要载体和支撑，是郑洛新国家自主创新示范区的重要组成部分，也是学校服务河南和区域经济社会的重要窗口。郑大科技园紧紧围绕“一园多点”的战略布局，构建以科技创新平台、成果转化基地、师生众创空间、科技企业孵化以及投融资服务等为主要内涵的创新创业全功能链。</p>', '2021-01-05 02:25:15', '2021-01-05 09:16:54');

-- --------------------------------------------------------

--
-- 表的结构 `fa_attachment`
--

CREATE TABLE IF NOT EXISTS `fa_attachment` (
  `id` int(20) unsigned NOT NULL COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件名称',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件 sha1编码'
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件表';

--
-- 转存表中的数据 `fa_attachment`
--

INSERT INTO `fa_attachment` (`id`, `admin_id`, `user_id`, `url`, `imagewidth`, `imageheight`, `imagetype`, `imageframes`, `filename`, `filesize`, `mimetype`, `extparam`, `createtime`, `updatetime`, `uploadtime`, `storage`, `sha1`) VALUES
(1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 'qrcode.png', 21859, 'image/png', '', 1499681848, 1499681848, 1499681848, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6'),
(2, 1, 0, '/uploads/20210104/fb473e4fd3df0354e4aa8478ee0dfab5.jpg', '520', '281', 'jpg', 0, '11111111111111111.jpg', 50909, 'image/jpeg', '', 1609726273, 1609726273, 1609726273, 'local', '02e9050088f7802eb9d0400455775a0064f36834'),
(3, 1, 0, '/uploads/20210104/0ede3d0a26f5d5668187b401a8a5eb81.png', '350', '350', 'png', 0, 'ba6ca4ef98980f81455b0ad814599eb7.png', 63683, 'image/png', '', 1609727325, 1609727325, 1609727325, 'local', 'e6f763cef870ca2755ec45c712bc51c993e28a81'),
(4, 1, 0, '/uploads/20210104/a4b9b78965325a2babe7a5d440df765a.png', '200', '200', 'png', 0, 'QQ.png', 5955, 'image/png', '', 1609727788, 1609727788, 1609727788, 'local', '75e39b6953286456e2d1efac0d4a19bfd6c16515'),
(5, 1, 0, '/uploads/20210104/7f17e4dfe9cea648d6cd090a684f78fe.png', '200', '200', 'png', 0, 'QQ (1).png', 6443, 'image/png', '', 1609727832, 1609727832, 1609727832, 'local', 'e978084dea7e1952d16ebe55acd54ff0f8606d84'),
(6, 1, 0, '/uploads/20210104/34550b001401852b726342469da3b7d7.jpg', '1905', '400', 'jpg', 0, '1609728690(1).jpg', 228038, 'image/jpeg', '', 1609728717, 1609728717, 1609728716, 'local', '103ca4c14829ded5aa7947ceb1bead1215cf8c76'),
(7, 1, 0, '/uploads/20210104/763ddb073c4e17b61380f5f89f1dfcb3.jpg', '1888', '126', 'jpg', 0, '1609728817(1).jpg', 226788, 'image/jpeg', '', 1609728826, 1609728826, 1609728826, 'local', 'fe42facaa095fd5628220efe7160f855fd4080c7'),
(8, 1, 0, '/uploads/20210104/888105480ed88c274aa6c8ae03dc64a3.jpg', '1805', '564', 'jpg', 0, '1609728913(1).jpg', 793669, 'image/jpeg', '', 1609728923, 1609728923, 1609728923, 'local', '9d22d613a2b3bd9d902de84abc15b25b8b9fa284'),
(9, 1, 0, '/uploads/20210104/ab26520f41a81ef5eb3ff3a921fffdc8.jpg', '799', '630', 'jpg', 0, '1609731395.jpg', 975824, 'image/jpeg', '', 1609731403, 1609731403, 1609731403, 'local', '2eb98178f2722019d0dccdc347d896eadfa58e94'),
(10, 1, 0, '/uploads/20210104/bd5816dc48eb4165b0363cad44834d98.jpg', '712', '368', 'jpg', 0, '1609744546(1).jpg', 441053, 'image/jpeg', '', 1609744551, 1609744551, 1609744551, 'local', '3cf74a8566ccc2c1509c2fdbd884d1f24832d710'),
(11, 1, 0, '/uploads/20210104/a5338098974474a991d6538edeea084c.jpg', '1782', '447', 'jpg', 0, '1609745432(1).jpg', 491522, 'image/jpeg', '', 1609745457, 1609745457, 1609745457, 'local', '03186c1cbf87be9e2a7fa4b2ac61e9d69e473dcc'),
(12, 1, 0, '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '459', '266', 'jpg', 0, '1609745854(1).jpg', 265149, 'image/jpeg', '', 1609745868, 1609745868, 1609745868, 'local', '50a6505905ec55228a299715597aab88b0142b6c'),
(13, 1, 0, '/uploads/20210104/e2b90caa6a24516794d9629b8bf38e65.png', '1493', '322', 'png', 0, '1609746597(1).png', 245073, 'image/png', '', 1609746617, 1609746617, 1609746617, 'local', 'dd76efec11b4e20eace86880cc5fd43c007b9e57'),
(14, 1, 0, '/uploads/20210104/8b97b628a9148e6c0e876de6d42bd6f6.jpg', '1479', '221', 'jpg', 0, '1609746788(1).jpg', 105390, 'image/jpeg', '', 1609746803, 1609746803, 1609746803, 'local', '9573d46edbbe637101ba6b52226c579894b0779d'),
(15, 1, 0, '/uploads/20210104/2332cee929b416037e7fd712857ab02a.jpg', '1518', '514', 'jpg', 0, '1609746820(1).jpg', 34328, 'image/jpeg', '', 1609746831, 1609746831, 1609746831, 'local', '371f49c956b6556c6e2d6cf9705617763499c2b2'),
(16, 1, 0, '/uploads/20210104/064a1c1ce3d68bea5446b2e1df8274c6.png', '1503', '712', 'png', 0, 'image.png', 611738, 'image/png', '', 1609749883, 1609749883, 1609749883, 'local', 'eff0a20c3a6e649b9e30a57144a1abe40a7149e6'),
(17, 1, 0, '/uploads/20210104/c7face2dca9c754812c86430c37ba730.png', '1509', '782', 'png', 0, 'image.png', 678341, 'image/png', '', 1609749898, 1609749898, 1609749898, 'local', 'd35da82ce6a3a0e11a252966b26e44450cbab1ac'),
(18, 1, 0, '/uploads/20210104/045ff1288598cd5f965acd6edbe249bf.png', '1504', '697', 'png', 0, 'image.png', 1001167, 'image/png', '', 1609749915, 1609749915, 1609749915, 'local', '3a181a3c799c8ba1631cea278e836fac1b9ba699'),
(19, 1, 0, '/uploads/20210104/a574a668b521925a06d7ab647b25bdbb.png', '1489', '647', 'png', 0, 'image.png', 579485, 'image/png', '', 1609750482, 1609750482, 1609750482, 'local', '6dfda15b401616c13930e2c1a27ebaf063df6cd3'),
(20, 1, 0, '/uploads/20210104/a37e31a5e361071c52f4ad4d0128e0ff.png', '1478', '777', 'png', 0, 'image.png', 668967, 'image/png', '', 1609750491, 1609750491, 1609750491, 'local', 'b08b850c2f63386fcf600360c23a8d931cdf1507'),
(21, 1, 0, '/uploads/20210104/3a56deee31dbf9f57379df76591bb936.jpg', '1487', '669', 'jpg', 0, '1609750584(1).jpg', 551308, 'image/jpeg', '', 1609750592, 1609750592, 1609750592, 'local', '0f4d2c8f73beb0163d39ed628b21f3ee03700cd6'),
(22, 1, 0, '/uploads/20210104/c5713d6128685bb772e6b48ad9f0f376.jpg', '1507', '799', 'jpg', 0, '1609750564.jpg', 663010, 'image/jpeg', '', 1609750601, 1609750601, 1609750601, 'local', '1183c888dce936628b7351f2363b7d4255df6d94'),
(23, 1, 0, '/uploads/20210104/e80c04aeb64e5c8f7fd2ca6ee11dec05.png', '1507', '799', 'png', 0, 'image.png', 679132, 'image/png', '', 1609750644, 1609750644, 1609750644, 'local', '62b1bcc10854885363c323fa68bb61b825c22619'),
(24, 1, 0, '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '255', '263', 'jpg', 0, '1609750786(1).jpg', 37845, 'image/jpeg', '', 1609750799, 1609750799, 1609750799, 'local', '1ef4408ff76fdd2c7dd88b3cee187d584e4143e1'),
(25, 1, 0, '/uploads/20210104/8ff733b93e223017ab43e47e85ff319a.jpg', '470', '363', 'jpg', 0, '1609751448(1).jpg', 412694, 'image/jpeg', '', 1609751456, 1609751456, 1609751456, 'local', 'f6e72dfa320b899df6c31d7b014a8c38c0e87409'),
(26, 1, 0, '/uploads/20210105/ee45c819d46c42812c02a84ea52b6d4e.jpg', '429', '323', 'jpg', 0, '111.JPG', 23099, 'image/jpeg', '', 1609819288, 1609819288, 1609819288, 'local', '71ebb01ca57f5007c771ade7d60fc33fd1961ba6'),
(27, 1, 0, '/uploads/20210105/15b8a5023701d6063ab5178223941c5a.jpg', '643', '585', 'jpg', 0, '222.JPG', 108196, 'image/jpeg', '', 1609820589, 1609820589, 1609820589, 'local', '5e674ccbf4f8d7c1262a1c3f8a8751577182f053'),
(28, 1, 0, '/uploads/20210105/9f80c627bdb192ef4a7ca0a641b35ad5.jpg', '1315', '619', 'jpg', 0, '22.JPG', 101144, 'image/jpeg', '', 1609824515, 1609824515, 1609824515, 'local', 'a233567b13415338e42ad7c3b334888f35e9effa'),
(29, 1, 0, '/uploads/20210105/1dc94bb480e19fa7ec6af2dce1b02098.jpg', '1851', '386', 'jpg', 0, '1609830140(1).jpg', 441254, 'image/jpeg', '', 1609830157, 1609830157, 1609830157, 'local', 'aa39d945dd34462276c0c1cb975bf3510462c82b'),
(30, 1, 0, '/uploads/20210105/88c53449a7cddb06af7662bec707838c.jpg', '400', '400', 'jpg', 0, 'SL150-KD-motorcycle-motorbike-400-400.jpg', 21252, 'image/jpeg', '', 1609836368, 1609836368, 1609836368, 'local', '0dfef41ccc06ce147669501b7ca64c0a7d570135'),
(31, 1, 0, '/uploads/20210105/9eb738f4917e2956b157e7f2d693e79f.jpg', '651', '465', 'jpg', 0, '360截图20210105170200258.jpg', 53411, 'image/jpeg', '', 1609837567, 1609837567, 1609837567, 'local', '4cbf65a6b605e18ed688094674489ee502ab180d'),
(32, 1, 0, '/uploads/20210105/79414a31813e147cedd1d071bbf0e406.jpg', '1161', '369', 'jpg', 0, '1609838052(1).jpg', 536331, 'image/jpeg', '', 1609838056, 1609838056, 1609838056, 'local', '2f23af907784cf2b4c25dcd3fd28eed7b3cc1f4a'),
(33, 1, 0, '/uploads/20210105/057c884acd2c865acbe741a039bb859e.jpg', '1920', '537', 'jpg', 0, '5811dcb4723b4_r9ml.jpg', 469379, 'image/jpeg', '', 1609848562, 1609848562, 1609848562, 'local', '6efe27d48514cd804aeb4521014a931b9641c7f1'),
(34, 1, 0, '/uploads/20210105/c23f3f59eb2e349622644accc569f061.png', '750', '750', 'png', 0, 'tupian3.png', 397584, 'image/png', '', 1609848677, 1609848677, 1609848677, 'local', '07ae59c0f7e1c8cb2a2c339d26f6a0ab4f556d20'),
(35, 1, 0, '/uploads/20210105/be28eb0e6c800a654755c451fd21b217.jpg', '1364', '256', 'jpg', 0, '360截图20210105201450229.jpg', 34386, 'image/jpeg', '', 1609848936, 1609848936, 1609848936, 'local', 'f8594be815660b87aa4c37612c204f30ba5d4d59'),
(36, 1, 0, '/uploads/20210105/12dfd8f5f3ff2f9b176d3bd998dc919d.jpg', '653', '455', 'jpg', 0, '360截图20210105203028758.jpg', 76651, 'image/jpeg', '', 1609849846, 1609849846, 1609849846, 'local', '2318ba7ba1edbe6379f1d5f879a12cab68f782f4'),
(37, 1, 0, '/uploads/20210105/25ad904a44365947d057a8e687d675ab.jpg', '1345', '575', 'jpg', 0, '360截图20210105205130673.jpg', 118764, 'image/jpeg', '', 1609851643, 1609851643, 1609851643, 'local', '7ab7f07e5fb87aaa09ef624fe20d40765bbfcc90'),
(38, 1, 0, '/uploads/20210105/cf67af91f034f49b1a1f774d71423dbb.jpg', '1920', '942', 'jpg', 0, '16741339169_567707692.jpg', 293279, 'image/jpeg', '', 1609851890, 1609851890, 1609851890, 'local', '3d9dca2ad1306a9b7f5eb054b77a39df5c5762de'),
(39, 1, 0, '/uploads/20210105/846e600751f159a7553d11cefef294d0.jpg', '1920', '707', 'jpg', 0, 'ba201901040942505521037.jpg', 130034, 'image/jpeg', '', 1609852424, 1609852424, 1609852424, 'local', 'f3b86b1c147183952b654d68f44956ad1e166fc8'),
(40, 1, 0, '/uploads/20210105/5d4702b79657455a8daf2415022388b4.jpg', '1330', '82', 'jpg', 0, '360截图20210105213157407.jpg', 8706, 'image/jpeg', '', 1609853556, 1609853556, 1609853556, 'local', '9bb57c0942e18e8bd33bdf269f1dd4293db046d0'),
(41, 1, 0, '/uploads/20210106/e1751ab5a7452b0dbbdf6ec462903e69.jpg', '1347', '391', 'jpg', 0, '360截图20210105212712090.jpg', 44607, 'image/jpeg', '', 1609905909, 1609905909, 1609905909, 'local', '8712f4717286c9ccab80d6b69d9aa407cb44c2ce');

-- --------------------------------------------------------

--
-- 表的结构 `fa_auth_group`
--

CREATE TABLE IF NOT EXISTS `fa_auth_group` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分组表';

--
-- 转存表中的数据 `fa_auth_group`
--

INSERT INTO `fa_auth_group` (`id`, `pid`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, 0, 'Admin group', '*', 1490883540, 149088354, 'normal'),
(2, 1, 'Second group', '13,14,16,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,1,9,10,11,7,6,8,2,4,5', 1490883540, 1505465692, 'normal'),
(3, 2, 'Third group', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,5', 1490883540, 1502205322, 'normal'),
(4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1490883540, 1502205350, 'normal'),
(5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1490883540, 1502205344, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_auth_group_access`
--

CREATE TABLE IF NOT EXISTS `fa_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限分组表';

--
-- 转存表中的数据 `fa_auth_group_access`
--

INSERT INTO `fa_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `fa_auth_rule`
--

CREATE TABLE IF NOT EXISTS `fa_auth_rule` (
  `id` int(10) unsigned NOT NULL,
  `type` enum('menu','file') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图标',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '条件',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='节点表';

--
-- 转存表中的数据 `fa_auth_rule`
--

INSERT INTO `fa_auth_rule` (`id`, `type`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', 'Dashboard tips', 0, 1497429920, 1609811692, 137, 'normal'),
(2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', 0, 1497429920, 1609726145, 119, 'normal'),
(3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', 'Category tips', 0, 1497429920, 1609743050, 99, 'normal'),
(4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', 'Addon tips', 0, 1502035509, 1609811662, 0, 'normal'),
(5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', 1, 1497429920, 1497430092, 60, 'normal'),
(6, 'file', 0, 'general/config', '系统配置', 'fa fa-cog', '', 'Config tips', 1, 1497429920, 1609726153, 143, 'normal'),
(7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', 'Attachment tips', 1, 1497429920, 1497430699, 53, 'normal'),
(8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', 1, 1497429920, 1497429920, 34, 'normal'),
(9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', 'Admin tips', 1, 1497429920, 1497430320, 118, 'normal'),
(10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', 'Admin log tips', 1, 1497429920, 1497430307, 113, 'normal'),
(11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', 'Group tips', 1, 1497429920, 1497429920, 109, 'normal'),
(12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', 'Rule tips', 1, 1497429920, 1497430581, 104, 'normal'),
(13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 136, 'normal'),
(14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 135, 'normal'),
(15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 133, 'normal'),
(16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 134, 'normal'),
(17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 132, 'normal'),
(18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 52, 'normal'),
(19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 51, 'normal'),
(20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 50, 'normal'),
(21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 49, 'normal'),
(22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 48, 'normal'),
(23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', 'Attachment tips', 0, 1497429920, 1497429920, 59, 'normal'),
(24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 58, 'normal'),
(25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 57, 'normal'),
(26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 56, 'normal'),
(27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 55, 'normal'),
(28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 54, 'normal'),
(29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 33, 'normal'),
(30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 32, 'normal'),
(31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 31, 'normal'),
(32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 30, 'normal'),
(33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 29, 'normal'),
(34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 28, 'normal'),
(35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', 'Category tips', 0, 1497429920, 1497429920, 142, 'normal'),
(36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 141, 'normal'),
(37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 140, 'normal'),
(38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 139, 'normal'),
(39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 138, 'normal'),
(40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', 'Admin tips', 0, 1497429920, 1497429920, 117, 'normal'),
(41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 116, 'normal'),
(42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 115, 'normal'),
(43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 114, 'normal'),
(44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', 'Admin log tips', 0, 1497429920, 1497429920, 112, 'normal'),
(45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 111, 'normal'),
(46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 110, 'normal'),
(47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', 'Group tips', 0, 1497429920, 1497429920, 108, 'normal'),
(48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 107, 'normal'),
(49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 106, 'normal'),
(50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 105, 'normal'),
(51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', 'Rule tips', 0, 1497429920, 1497429920, 103, 'normal'),
(52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 102, 'normal'),
(53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 101, 'normal'),
(54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 100, 'normal'),
(55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', 'Addon tips', 0, 1502035509, 1502035509, 0, 'normal'),
(56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', 0, 1516374729, 1609726155, 0, 'normal'),
(67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(102, 'file', 0, 'icon', '图标管理', 'fa fa-circle-o\r', '', '', 1, 1609727230, 1609727230, 0, 'normal'),
(103, 'file', 102, 'icon/import', 'Import', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(104, 'file', 102, 'icon/index', '查看', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(105, 'file', 102, 'icon/add', '添加', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(106, 'file', 102, 'icon/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(107, 'file', 102, 'icon/del', '删除', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(108, 'file', 102, 'icon/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1609727230, 1609727314, 0, 'normal'),
(109, 'file', 0, 'image', '图片管理', 'fa fa-image\r', '', '', 1, 1609741983, 1609741983, 0, 'normal'),
(110, 'file', 109, 'image/import', 'Import', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(111, 'file', 109, 'image/index', '查看', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(112, 'file', 109, 'image/add', '添加', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(113, 'file', 109, 'image/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(114, 'file', 109, 'image/del', '删除', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(115, 'file', 109, 'image/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1609741983, 1609742993, 0, 'normal'),
(123, 'file', 0, 'article', '文本管理', 'fa fa-circle-o\r', '', '', 1, 1609747441, 1609747441, 0, 'normal'),
(124, 'file', 123, 'article/import', 'Import', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(125, 'file', 123, 'article/index', '查看', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(126, 'file', 123, 'article/add', '添加', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(127, 'file', 123, 'article/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(128, 'file', 123, 'article/del', '删除', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(129, 'file', 123, 'article/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1609747441, 1609747529, 0, 'normal'),
(144, 'file', 0, 'message', '留言管理', 'fa fa-circle-o\r', '', '', 1, 1609811642, 1609811642, 0, 'normal'),
(145, 'file', 144, 'message/import', 'Import', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(146, 'file', 144, 'message/index', '查看', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(147, 'file', 144, 'message/add', '添加', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(148, 'file', 144, 'message/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(149, 'file', 144, 'message/del', '删除', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(150, 'file', 144, 'message/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1609811642, 1609811642, 0, 'normal'),
(151, 'file', 0, 'about', '关于我们', 'fa fa-circle-o\r', '', '', 1, 1609816850, 1609816850, 0, 'normal'),
(152, 'file', 151, 'about/import', 'Import', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal'),
(153, 'file', 151, 'about/index', '查看', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal'),
(154, 'file', 151, 'about/add', '添加', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal'),
(155, 'file', 151, 'about/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal'),
(156, 'file', 151, 'about/del', '删除', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal'),
(157, 'file', 151, 'about/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1609816850, 1609816850, 0, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_category`
--

CREATE TABLE IF NOT EXISTS `fa_category` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分类表';

--
-- 转存表中的数据 `fa_category`
--

INSERT INTO `fa_category` (`id`, `pid`, `type`, `name`, `nickname`, `flag`, `image`, `keywords`, `description`, `diyname`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 0, 'page', '官方新闻', 'news', 'recommend', '/assets/img/qrcode.png', '', '', 'news', 1495262190, 1495262190, 1, 'normal'),
(2, 0, 'page', '移动应用', 'mobileapp', 'hot', '/assets/img/qrcode.png', '', '', 'mobileapp', 1495262244, 1495262244, 2, 'normal'),
(3, 2, 'page', '微信公众号', 'wechatpublic', 'index', '/assets/img/qrcode.png', '', '', 'wechatpublic', 1495262288, 1495262288, 3, 'normal'),
(4, 2, 'page', 'Android开发', 'android', 'recommend', '/assets/img/qrcode.png', '', '', 'android', 1495262317, 1495262317, 4, 'normal'),
(5, 0, 'page', '软件产品', 'software', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1495262336, 1499681850, 5, 'normal'),
(6, 5, 'page', '网站建站', 'website', 'recommend', '/assets/img/qrcode.png', '', '', 'website', 1495262357, 1495262357, 6, 'normal'),
(7, 5, 'page', '企业管理软件', 'company', 'index', '/assets/img/qrcode.png', '', '', 'company', 1495262391, 1495262391, 7, 'normal'),
(8, 6, 'page', 'PC端', 'website-pc', 'recommend', '/assets/img/qrcode.png', '', '', 'website-pc', 1495262424, 1495262424, 8, 'normal'),
(9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1495262456, 1495262456, 9, 'normal'),
(10, 7, 'page', 'CRM系统 ', 'company-crm', 'recommend', '/assets/img/qrcode.png', '', '', 'company-crm', 1495262487, 1495262487, 10, 'normal'),
(11, 7, 'page', 'SASS平台软件', 'company-sass', 'recommend', '/assets/img/qrcode.png', '', '', 'company-sass', 1495262515, 1495262515, 11, 'normal'),
(12, 0, 'test', '测试1', 'test1', 'recommend', '/assets/img/qrcode.png', '', '', 'test1', 1497015727, 1497015727, 12, 'normal'),
(13, 0, 'test', '测试2', 'test2', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1497015738, 1497015738, 13, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_config`
--

CREATE TABLE IF NOT EXISTS `fa_config` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量名',
  `group` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '分组',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text COLLATE utf8mb4_unicode_ci COMMENT '变量值',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '变量字典数据',
  `rule` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '扩展属性',
  `setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置'
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统配置';

--
-- 转存表中的数据 `fa_config`
--

INSERT INTO `fa_config` (`id`, `name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`, `setting`) VALUES
(1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', 'xxxx设备有限公司', '', 'required', '', NULL),
(2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '豫A888888', '', '', '', NULL),
(6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', '', NULL),
(8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', '', NULL),
(18, 'desc', 'basic', '站点描述', '请填写站点描述', 'string', '描述', '', 'required', '', NULL),
(19, 'keywords', 'basic', '关键词', '请填写站点关键词', 'string', '关键词，关键词2', '', 'required', '', NULL),
(20, 'logo', 'basic', 'Logo', '请上传Logo横图', 'image', '/uploads/20210105/5d4702b79657455a8daf2415022388b4.jpg', '', 'required', '', NULL),
(21, 'banner', 'basic', 'Banner', '请上传Banner', 'image', '/uploads/20210105/cf67af91f034f49b1a1f774d71423dbb.jpg', '', 'required', '', NULL),
(23, 'admin_email', 'basic', 'Email', '请填写Email', 'string', '123456789@qq.com', '', 'required', '', NULL),
(25, 'configgroup', 'dictionary', 'Config group', '', 'array', '{"basic":"Basic","about":"关于我们","dictionary":"Dictionary"}', '', '', '', NULL),
(27, 'about_img', 'about', '关于我们大图', '请上传关于我们大图', 'image', '/uploads/20210105/12dfd8f5f3ff2f9b176d3bd998dc919d.jpg', '', 'required', '', NULL),
(28, 'FAQ', 'faq', 'FAQ', '请填写FAQ', 'text', '', '', 'required', '', NULL),
(29, 'about_text', 'about', '关于我们文字', '请填写关于我们文字', 'text', '无', '', 'required', '', NULL),
(30, 'footer_img', 'basic', '底部横图', '请上传底部横图', 'image', '/uploads/20210106/e1751ab5a7452b0dbbdf6ec462903e69.jpg', '', 'required', '', NULL),
(31, 'honour_img', 'basic', '荣誉资质横图', '请上传荣誉资质横图', 'image', '/uploads/20210105/cf67af91f034f49b1a1f774d71423dbb.jpg', '', 'required', '', NULL),
(32, 'packing_img', 'basic', '产品包装横图', '请上传产品包装横图', 'image', '/uploads/20210105/cf67af91f034f49b1a1f774d71423dbb.jpg', '', 'required', '', NULL),
(33, 'statistics', 'basic', '统计代码', '请填写统计代码', 'text', '<script></script>', '', 'required', '', NULL),
(34, 'device_img', 'basic', '生产设备横图', '请上传生产设备横图', 'image', '/uploads/20210105/25ad904a44365947d057a8e687d675ab.jpg', '', 'required', '', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `fa_ems`
--

CREATE TABLE IF NOT EXISTS `fa_ems` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='邮箱验证码表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_icon`
--

CREATE TABLE IF NOT EXISTS `fa_icon` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '小图标',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '跳转地址',
  `title` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '显示名称',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='图标管理';

--
-- 转存表中的数据 `fa_icon`
--

INSERT INTO `fa_icon` (`id`, `icon`, `url`, `title`, `created_at`, `updated_at`) VALUES
(3, '/uploads/20210104/7f17e4dfe9cea648d6cd090a684f78fe.png', 'http://www.baidu.com', '小图标', '2021-01-04 02:36:23', '2021-01-04 02:45:04'),
(4, '/uploads/20210104/7f17e4dfe9cea648d6cd090a684f78fe.png', 'http://www.baidu.com', '小图标', '2021-01-04 02:37:48', '2021-01-04 02:45:00');

-- --------------------------------------------------------

--
-- 表的结构 `fa_image`
--

CREATE TABLE IF NOT EXISTS `fa_image` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片',
  `p1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文字1',
  `p2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文字2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='图片管理';

--
-- 转存表中的数据 `fa_image`
--

INSERT INTO `fa_image` (`id`, `category`, `image`, `p1`, `p2`, `created_at`, `updated_at`) VALUES
(1, '关于我们', '/uploads/20210104/bd5816dc48eb4165b0363cad44834d98.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:14:49', '2021-01-04 07:14:49'),
(2, '关于我们', '/uploads/20210104/bd5816dc48eb4165b0363cad44834d98.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:16:50', '2021-01-04 07:16:50'),
(3, '关于我们', '/uploads/20210104/bd5816dc48eb4165b0363cad44834d98.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:28:44', '2021-01-04 07:28:44'),
(4, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:21', '2021-01-04 07:37:21'),
(5, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:53', '2021-01-04 07:37:53'),
(6, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:21', '2021-01-04 07:37:21'),
(7, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:53', '2021-01-04 07:37:53'),
(8, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:21', '2021-01-04 07:37:21'),
(9, '生产设备', '/uploads/20210104/3dd73a99cc1d7329fdc1db20378eed2b.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:37:53', '2021-01-04 07:37:53'),
(10, '荣誉资质', '/uploads/20210105/cf67af91f034f49b1a1f774d71423dbb.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 07:50:08', '2021-01-05 13:04:53'),
(11, '荣誉资质', '/uploads/20210105/be28eb0e6c800a654755c451fd21b217.jpg', '', '', '2021-01-04 07:53:14', '2021-01-05 12:15:41'),
(12, '荣誉资质', '/uploads/20210105/9f80c627bdb192ef4a7ca0a641b35ad5.jpg', '', '', '2021-01-04 07:53:45', '2021-01-05 05:28:36'),
(13, '产品详情', '/uploads/20210105/88c53449a7cddb06af7662bec707838c.jpg', '先进的生产设备', 'Advanced production equipment', '2021-01-04 08:59:35', '2021-01-05 08:46:26'),
(14, '产品详情', '/uploads/20210105/88c53449a7cddb06af7662bec707838c.jpg', '先进的生产设备', 'Advanced production equipment', '2021-01-04 08:59:35', '2021-01-05 08:47:27'),
(15, '产品详情', '/uploads/20210105/88c53449a7cddb06af7662bec707838c.jpg', '先进的生产设备', 'Advanced production equipment', '2021-01-04 08:59:35', '2021-01-05 08:47:55'),
(16, '产品详情', '/uploads/20210105/88c53449a7cddb06af7662bec707838c.jpg', '先进的生产设备', 'Advanced production equipment', '2021-01-04 08:59:35', '2021-01-05 08:48:18'),
(17, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(18, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(19, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(20, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(21, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(22, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(23, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', '先进的生产设备', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-04 08:59:35'),
(24, '产品详情', '/uploads/20210104/fa13f1ce9d1889a21571b1d759f7d273.jpg', 'TEST', '先进的生产设备', '2021-01-04 08:59:35', '2021-01-05 04:00:28'),
(25, '物流运输', '/uploads/20210104/8ff733b93e223017ab43e47e85ff319a.jpg', '', '', '2021-01-04 09:10:37', '2021-01-04 09:10:37'),
(26, '物流运输', '/uploads/20210104/8ff733b93e223017ab43e47e85ff319a.jpg', '', '', '2021-01-04 09:11:00', '2021-01-04 09:11:00'),
(27, '物流运输', '/uploads/20210104/8ff733b93e223017ab43e47e85ff319a.jpg', '', '', '2021-01-04 09:11:07', '2021-01-04 09:11:07'),
(28, '产品详情', '/uploads/20210105/ee45c819d46c42812c02a84ea52b6d4e.jpg', 'TEST 2', '测试2', '2021-01-05 04:00:40', '2021-01-05 04:00:40');

-- --------------------------------------------------------

--
-- 表的结构 `fa_message`
--

CREATE TABLE IF NOT EXISTS `fa_message` (
  `id` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公司名称',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '联系人',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '电话',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱',
  `msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '留言',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='留言管理';

--
-- 转存表中的数据 `fa_message`
--

INSERT INTO `fa_message` (`id`, `company`, `name`, `phone`, `email`, `msg`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '1', '1', '1', '2021-01-05 02:19:21', '2021-01-05 02:19:21');

-- --------------------------------------------------------

--
-- 表的结构 `fa_sms`
--

CREATE TABLE IF NOT EXISTS `fa_sms` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'IP',
  `createtime` int(10) unsigned DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='短信验证码表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_test`
--

CREATE TABLE IF NOT EXISTS `fa_test` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID(单选)',
  `category_ids` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片',
  `images` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关键字',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '描述',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '省市',
  `json` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `price` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击',
  `startdate` date DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year(4) DEFAULT NULL COMMENT '年',
  `times` time DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) DEFAULT NULL COMMENT '刷新时间(int)',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='测试表';

--
-- 转存表中的数据 `fa_test`
--

INSERT INTO `fa_test` (`id`, `admin_id`, `category_id`, `category_ids`, `week`, `flag`, `genderdata`, `hobbydata`, `title`, `content`, `image`, `images`, `attachfile`, `keywords`, `description`, `city`, `json`, `price`, `views`, `startdate`, `activitytime`, `year`, `times`, `refreshtime`, `createtime`, `updatetime`, `deletetime`, `weigh`, `switch`, `status`, `state`) VALUES
(1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', '{"a":"1","b":"2"}', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1499682285, 1499682526, 1499682526, NULL, 0, 1, 'normal', '1');

-- --------------------------------------------------------

--
-- 表的结构 `fa_user`
--

CREATE TABLE IF NOT EXISTS `fa_user` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '昵称',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Token',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态',
  `verification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员表';

--
-- 转存表中的数据 `fa_user`
--

INSERT INTO `fa_user` (`id`, `group_id`, `username`, `nickname`, `password`, `salt`, `email`, `mobile`, `avatar`, `level`, `gender`, `birthday`, `bio`, `money`, `score`, `successions`, `maxsuccessions`, `prevtime`, `logintime`, `loginip`, `loginfailure`, `joinip`, `jointime`, `createtime`, `updatetime`, `token`, `status`, `verification`) VALUES
(1, 1, 'admin', 'admin', '47b1575b52e0628b36dbbb7721f20be4', '9eec5f', 'admin@163.com', '13888888888', '', 0, 0, '2017-04-15', '', '0.00', 0, 1, 1, 1516170492, 1516171614, '127.0.0.1', 0, '127.0.0.1', 1491461418, 0, 1516171614, '', 'normal', '');

-- --------------------------------------------------------

--
-- 表的结构 `fa_user_group`
--

CREATE TABLE IF NOT EXISTS `fa_user_group` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '组名',
  `rules` text COLLATE utf8mb4_unicode_ci COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员组表';

--
-- 转存表中的数据 `fa_user_group`
--

INSERT INTO `fa_user_group` (`id`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1515386468, 1516168298, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_user_money_log`
--

CREATE TABLE IF NOT EXISTS `fa_user_money_log` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员余额变动表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_user_rule`
--

CREATE TABLE IF NOT EXISTS `fa_user_rule` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '标题',
  `remark` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员规则表';

--
-- 转存表中的数据 `fa_user_rule`
--

INSERT INTO `fa_user_rule` (`id`, `pid`, `name`, `title`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 0, 'index', 'Frontend', '', 1, 1516168079, 1516168079, 1, 'normal'),
(2, 0, 'api', 'API Interface', '', 1, 1516168062, 1516168062, 2, 'normal'),
(3, 1, 'user', 'User Module', '', 1, 1515386221, 1516168103, 12, 'normal'),
(4, 2, 'user', 'User Module', '', 1, 1515386221, 1516168092, 11, 'normal'),
(5, 3, 'index/user/login', 'Login', '', 0, 1515386247, 1515386247, 5, 'normal'),
(6, 3, 'index/user/register', 'Register', '', 0, 1515386262, 1516015236, 7, 'normal'),
(7, 3, 'index/user/index', 'User Center', '', 0, 1516015012, 1516015012, 9, 'normal'),
(8, 3, 'index/user/profile', 'Profile', '', 0, 1516015012, 1516015012, 4, 'normal'),
(9, 4, 'api/user/login', 'Login', '', 0, 1515386247, 1515386247, 6, 'normal'),
(10, 4, 'api/user/register', 'Register', '', 0, 1515386262, 1516015236, 8, 'normal'),
(11, 4, 'api/user/index', 'User Center', '', 0, 1516015012, 1516015012, 10, 'normal'),
(12, 4, 'api/user/profile', 'Profile', '', 0, 1516015012, 1516015012, 3, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `fa_user_score_log`
--

CREATE TABLE IF NOT EXISTS `fa_user_score_log` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT '0' COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT '0' COMMENT '变更后积分',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员积分变动表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_user_token`
--

CREATE TABLE IF NOT EXISTS `fa_user_token` (
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(10) DEFAULT NULL COMMENT '过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员Token表';

-- --------------------------------------------------------

--
-- 表的结构 `fa_version`
--

CREATE TABLE IF NOT EXISTS `fa_version` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `oldversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '包大小',
  `content` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='版本表';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fa_about`
--
ALTER TABLE `fa_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_admin`
--
ALTER TABLE `fa_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `fa_admin_log`
--
ALTER TABLE `fa_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`username`);

--
-- Indexes for table `fa_area`
--
ALTER TABLE `fa_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fa_article`
--
ALTER TABLE `fa_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_attachment`
--
ALTER TABLE `fa_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_auth_group`
--
ALTER TABLE `fa_auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_auth_group_access`
--
ALTER TABLE `fa_auth_group_access`
  ADD UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `fa_auth_rule`
--
ALTER TABLE `fa_auth_rule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`) USING BTREE,
  ADD KEY `pid` (`pid`),
  ADD KEY `weigh` (`weigh`);

--
-- Indexes for table `fa_category`
--
ALTER TABLE `fa_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `weigh` (`weigh`,`id`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fa_config`
--
ALTER TABLE `fa_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `fa_ems`
--
ALTER TABLE `fa_ems`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `fa_icon`
--
ALTER TABLE `fa_icon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_image`
--
ALTER TABLE `fa_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_message`
--
ALTER TABLE `fa_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_sms`
--
ALTER TABLE `fa_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_test`
--
ALTER TABLE `fa_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user`
--
ALTER TABLE `fa_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `fa_user_group`
--
ALTER TABLE `fa_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_money_log`
--
ALTER TABLE `fa_user_money_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_rule`
--
ALTER TABLE `fa_user_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_score_log`
--
ALTER TABLE `fa_user_score_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fa_user_token`
--
ALTER TABLE `fa_user_token`
  ADD PRIMARY KEY (`token`);

--
-- Indexes for table `fa_version`
--
ALTER TABLE `fa_version`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fa_about`
--
ALTER TABLE `fa_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fa_admin`
--
ALTER TABLE `fa_admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fa_admin_log`
--
ALTER TABLE `fa_admin_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=202;
--
-- AUTO_INCREMENT for table `fa_area`
--
ALTER TABLE `fa_area`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `fa_article`
--
ALTER TABLE `fa_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fa_attachment`
--
ALTER TABLE `fa_attachment`
  MODIFY `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `fa_auth_group`
--
ALTER TABLE `fa_auth_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fa_auth_rule`
--
ALTER TABLE `fa_auth_rule`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT for table `fa_category`
--
ALTER TABLE `fa_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `fa_config`
--
ALTER TABLE `fa_config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `fa_ems`
--
ALTER TABLE `fa_ems`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `fa_icon`
--
ALTER TABLE `fa_icon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fa_image`
--
ALTER TABLE `fa_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `fa_message`
--
ALTER TABLE `fa_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fa_sms`
--
ALTER TABLE `fa_sms`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `fa_test`
--
ALTER TABLE `fa_test`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fa_user`
--
ALTER TABLE `fa_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fa_user_group`
--
ALTER TABLE `fa_user_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fa_user_money_log`
--
ALTER TABLE `fa_user_money_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fa_user_rule`
--
ALTER TABLE `fa_user_rule`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `fa_user_score_log`
--
ALTER TABLE `fa_user_score_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fa_version`
--
ALTER TABLE `fa_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
